/*
 * Copyright (c) 2020. Nils Witt
 */

package de.nils_witt.splan;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    private final List<String> months = new ArrayList<>();

    public Utils() {
        months.add("Januar");
        months.add("Februar");
        months.add("März");
        months.add("April");
        months.add("Mai");
        months.add("Juni");
        months.add("Juli");
        months.add("August");
        months.add("September");
        months.add("Oktober");
        months.add("November");
        months.add("Dezember");
    }

    public String convertDate(String oldDate) {
        String[] parts = oldDate.split(" ");
        int monthInt = months.indexOf(parts[2]) + 1;
        String year = parts[3];
        String month = Integer.toString(monthInt);
        String day = parts[1].substring(0, parts[1].length() - 1);
        if(day.length() == 1){
            day = 0 + day;
        }
        System.out.println(year + "-" + month + "-" + day);
        return year + "-" + month + "-" + day;
    }
}
