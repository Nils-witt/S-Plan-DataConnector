
/*
 * Copyright (c) 2020. Nils Witt
 */

package de.nils_witt.splan.dataModels;

public class Aufsicht {
    private String time;
    private String teacher;
    private String location;

    public Aufsicht() {

    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setLocation(String location) {
        this.location = location;
    }


}
